#include <sourcemod>
#include <sdktools>

public void OnPluginStart()
{
	HookEvent("player_death", Event_OnPlayerDeath);
}

int g_iCameraRef[MAXPLAYERS+1] = {INVALID_ENT_REFERENCE, ...};

public Action Event_OnPlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (!IsValidEntity(client))
		return Plugin_Continue;
	
	int ragdoll = GetEntPropEnt(client, Prop_Send, "m_hRagdoll");
	if (ragdoll < 0)
		return Plugin_Continue;
	
	SpawnCamAndAttach(client, ragdoll);
	
	return Plugin_Continue;
}

stock bool SpawnCamAndAttach(int iClient, int iRagdoll)
{
	char sModel[64];
	Format(sModel, sizeof(sModel), "models/blackout.mdl"); //
	PrecacheModel(sModel, true);

	char sTargetName[64]; 
	Format(sTargetName, sizeof(sTargetName), "ragdoll%d", iClient);
	DispatchKeyValue(iRagdoll, "targetname", sTargetName);

	int iEntity = CreateEntityByName("prop_dynamic");
	if (iEntity == -1)
		return;

	char sCamName[64]; 
	Format(sCamName, sizeof(sCamName), "ragdollCam%d", iEntity);

	DispatchKeyValue(iEntity, "targetname", sCamName);
	DispatchKeyValue(iEntity, "parentname", sTargetName);
	DispatchKeyValue(iEntity, "model",	  sModel);
	DispatchKeyValue(iEntity, "solid",	  "0");
	DispatchKeyValue(iEntity, "rendermode", "10"); // dont render
	DispatchKeyValue(iEntity, "disableshadows", "1"); // no shadows

	float fAngles[3]; 
	GetClientEyeAngles(iClient, fAngles);
	
	char sCamAngles[64];
	Format(sCamAngles, 64, "%f %f %f", fAngles[0], fAngles[1], fAngles[2]);
	
	DispatchKeyValue(iEntity, "angles", sCamAngles);

	SetEntityModel(iEntity, sModel);
	DispatchSpawn(iEntity);

	SetVariantString(sTargetName);
	AcceptEntityInput(iEntity, "SetParent", iEntity, iEntity, 0);

	SetVariantString("facemask");
	AcceptEntityInput(iEntity, "SetParentAttachment", iEntity, iEntity, 0);

	AcceptEntityInput(iEntity, "TurnOn");

	SetClientViewEntity(iClient, iEntity);
	g_iCameraRef[iClient] = EntIndexToEntRef(iEntity);
	
	CreateTimer(3.0, Timer_ResetCam, iClient, TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_ResetCam(Handle timer, any iClient)
{
	if(IsClientInGame(iClient))
		SetClientViewEntity(iClient, iClient);
	
	int iEntity = EntRefToEntIndex(g_iCameraRef[iClient]);
	if(iEntity != INVALID_ENT_REFERENCE)
		AcceptEntityInput(iEntity, "kill");
	
	g_iCameraRef[iClient] = INVALID_ENT_REFERENCE;
	
	return Plugin_Handled;
}